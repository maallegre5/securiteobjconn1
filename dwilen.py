import asyncio
import argparse
from struct import unpack, pack
from collections import namedtuple
from datetime import datetime
from bleak import BleakScanner, BleakClient
from math import ceil
from bleak.backends.service import BleakGATTCharacteristic
from progress.bar import IncrementalBar

# Barre de progression pour le téléchargement
class FancyBar(IncrementalBar):
    suffix = '%(percent).1f%% - %(eta)ds'

# Callback pour lister les fichiers
def list_callback(sender: BleakGATTCharacteristic, data: bytearray):
    formatted_bytes = ['{:02X}'.format(b) for b in data]
    formatted_string = ' '.join(formatted_bytes)
    file_info = FileInfo._make(unpack('<8sIIQ', data))
    print("Fichier", file_info.name.decode('ascii'), "de taille" , file_info.size)
    print("CRC", file_info.crc)
    print("Créé le", datetime.fromtimestamp(file_info.creationDate / 1000))
    print("Infos brutes :", formatted_string)
    print("---")

# Callback qui liste les fichiers et les ajoute dans un dictionnaire
def list_callback_to_dict(filelist):
    def i_list_callback(sender: BleakGATTCharacteristic, data: bytearray):
        formatted_bytes = ['{:02X}'.format(b) for b in data]
        formatted_string = ' '.join(formatted_bytes)
        file_info = FileInfo._make(unpack('<8sIIQ', data))
        print("Fichier", file_info.name.decode('ascii'), "de taille" , file_info.size)
        print("CRC", file_info.crc)
        print("Créé le", datetime.fromtimestamp(file_info.creationDate / 1000))
        print("Infos brutes :", formatted_string)
        print("---")
        filelist[file_info.name.decode('ascii').replace("\x00","")] = file_info
    return i_list_callback

# Callback pour ACK
def ack_callback(sender: BleakGATTCharacteristic, data: bytearray):
    ack = Ack._make(unpack('<HHI20s', data))
    print("---")
    print("ACK reçu pour la caractéristique", ack.carId)
    print(f"Erreur {ack.err}, résultat {ack.res}")
    print(ack.mess.decode('ascii'))
    print("---")

# Conversion du nom d'un fichier en bytearray de la bonne taille pour un envoi sur READ
def str_to_bytes(chaine):
    return chaine.encode('ascii') + bytearray(8-len(chaine))

# Convertit un uuid court en long
def long_uuid(shortid):
    return "1b0d" + str(shortid) + "-a720-f7e9-46b6-31b601c4fca1"

# Callback pour READ, prend un fichier ouvert et une barre de progression
def read_callback(f, bar):
    def i_read_callback(sender: BleakGATTCharacteristic, data: bytearray):
        f.write(data)
        bar.next()
    return i_read_callback

# Callback générique pour les NOTIFY demandés par l'utilisateur
def gen_callback(sender: BleakGATTCharacteristic, data: bytearray):
    formatted_bytes = ['{:02X}'.format(b) for b in data]
    formatted_string = ' '.join(formatted_bytes)
    print("NOTIFY RECU")
    print("De :", sender)
    print("BRUT :", formatted_string)
    try:
        print("ASCII :", data.decode("ascii"))
    except Exception:
        print("Pas de conversion ASCII directe")
    print("---")

# Telecharge le fichier filename, avec filedata un FileInfo et client un BleakClient
async def download_file(filedata, filename, client):
    # si le fichier existe
    if filedata is not None:
        chunk_count = ceil(filedata.size / 230.0)
        with FancyBar(f" Téléchargement de {filename}...\t", max=chunk_count) as bar:
            request = pack("<8sII", str_to_bytes(filename), 0, 9999) # creation de la requete avec pack
            file_to_write = open(filename, "wb") # ouverture du fichier
            await client.start_notify(long_uuid(1304), read_callback(file_to_write, bar)) # abonnement à READ (1304) avec du curry pour passer le fichier
            await client.write_gatt_char(long_uuid(1304), request, response=True) # envoi de la requete sur READ
            while bar.remaining != 0:
                await asyncio.sleep(0.1)
            file_to_write.close()
    else:
        print(f"Le fichier {filename} n'existe pas")

async def main(address):
    #devices = await BleakScanner.discover()
    
    async with BleakClient(address) as client:
        filelist = dict()

        is_connected = client.is_connected
        if is_connected:
            # Lister les services
            # print("Connected to " + address + ".")
            # for key in client.services.services:
            #     print(key, client.services.services[key])

            # Afficher la liste des fichiers
            file_count = int.from_bytes(await client.read_gatt_char(long_uuid(1303)), "little") # recuperation nombre fichiers sur NUM (1303)
            print("Nombre de fichiers sur le Dwilen :", file_count)
            await client.start_notify(long_uuid(1302), list_callback_to_dict(filelist)) # abonnement à LIST (1302)
            await client.write_gatt_char(long_uuid(1302), int.to_bytes(file_count, 2, "little"), response=True) # envoi du nombre de fichiers sur LIST (1302)
            await asyncio.sleep(1) # attente pour avoir tout reçu

            await client.start_notify(long_uuid(1407), ack_callback) # abonnement à ACK (1307)

            while True:
                print("\nOptions:")
                print("1. READ")
                print("2. WRITE")
                print("3. NOTIFY")
                print("4. Lister les fichiers")
                print("5. Télécharger un fichier")
                print("6. Télécharger tous les fichiers")
                print("0. Quitter")
                choice = input("Entrer votre choix: ")

                if choice == '1':
                    char_uuid = input("UUID version courte à lire : ")
                    try: 
                        value = await client.read_gatt_char(long_uuid(char_uuid))
                        formatted_bytes = ['{:02X}'.format(b) for b in value]
                        formatted_string = ' '.join(formatted_bytes)
                        print(f"Valeur : {formatted_string}")
                    except Exception as e:
                        print(f"Echec de lecture : {e}")

                elif choice == '2':
                    char_uuid = input("UUID version courte à écrire : ")
                    value = input("Valeur hexa à envoyer : ")
                    try:
                        value_bytes = bytes.fromhex(value)
                        await client.write_gatt_char(long_uuid(char_uuid), value_bytes, response=True)
                        print("Envoi reussi")
                    except Exception as e:
                        print(f"Echec d'ecriture : {e}")
                    await asyncio.sleep(1) #attente pour recevoir le notif

                elif choice == '3':
                    char_uuid = input("UUID version courte où s'abonner : ")
                    try: 
                        await client.start_notify(long_uuid(char_uuid), gen_callback)
                    except Exception as e:
                        print(f"Echec de notify : {e}")

                elif choice == '4':
                    # Afficher la liste des fichiers
                    file_count = int.from_bytes(await client.read_gatt_char(long_uuid(1303)), "little") # recuperation nombre fichiers sur NUM (1303)
                    print("Nombre de fichiers sur le Dwilen :", file_count)
                    await client.start_notify(long_uuid(1302), list_callback) # abonnement à LIST (1302)
                    await client.write_gatt_char(long_uuid(1302), int.to_bytes(file_count, 2, "little"), response=True) # envoi du nombre de fichiers sur LIST (1302)
                    await asyncio.sleep(1) #attente pour recevoir le notify

                elif choice == '5':
                    await client.stop_notify(long_uuid(1407)) # desabonnement de ack pour eviter qu'il coupe la barre de progression
                    filename = input("Nom du fichier : ")
                    filedata = filelist.get(filename)
                    await download_file(filedata, filename, client)
                    await client.start_notify(long_uuid(1407), ack_callback) # reabonnement à ACK (1307)

                elif choice == '6':
                    await client.stop_notify(long_uuid(1407)) # desabonnement de ack pour eviter qu'il coupe la barre de progression
                    # Telechargement de tous les fichiers
                    for filename in filelist.keys():
                        filedata = filelist.get(filename)
                        await download_file(filedata, filename, client)
                    await client.start_notify(long_uuid(1407), ack_callback) # reabonnement à ACK (1307)
 
                elif choice == '0':
                    print("Arret de la connexion")
                    break
                else:
                    print("Choix inconnu")

        else:
            print("Could not connect to " + address + ".")

parser = argparse.ArgumentParser(
                    prog="python test.py",
                    description="Permet d'effectuer diverses operations sur un Dwilen")
parser.add_argument('macAddress', help="Adresse MAC du boitier")
args = parser.parse_args()

FileInfo = namedtuple('FileInfo', 'name size crc creationDate')
Ack = namedtuple('Ack', 'carId err res mess')

asyncio.run(main(args.macAddress))
