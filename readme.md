# Installation

```sh
pip install bleak=0.14.0
pip install asyncio
pip install progress
```

# Utilisation

Le script dwilen.py peut être utilisé de la façon suivante :

```sh
python dwilen.py <macaddress>
```

Par exemple :

```sh
python dwilen.py "A1:B2:C3:D4:E5:F6"
```

Le script affiche le nombre et la liste des fichiers sur le Dwilen, puis un menu interactif permet de lire, écrire ou se notifier sur une caractéristique. Des instructions plus complexes sont aussi disponibles : lister les fichiers, télécharger un fichier avec son nom et télécharger tous les fichiers.